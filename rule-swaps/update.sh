#!/bin/sh
# Automatically provisions or updates a server to deploy the RuleSwaps application

set -e
cd $(dirname $0)

# Create user if needed
if test $(set +e; id -u rule-swaps > /dev/null 2>&1; echo $?) = 1; then
	echo "Creating user rule-swaps"
	mkdir -p /home/rule-swaps
	useradd -r rule-swaps -d /home/rule-swaps
	chown rule-swaps /home/rule-swaps
fi

# Install dependencies
if test ! -f /usr/bin/node; then
	echo "Installing dependencies"
	apt-get -y install nodejs npm moreutils
	ln -s /usr/bin/nodejs /usr/bin/node
fi

# Update debug deployment
sh update-debug.sh