#!/bin/sh

set -e
cd $(dirname $0)

# Check for rule swaps service directory
if test -d /srv/debug/rule-swaps; then

	# Create debug deployment
	HOME_DIR=~rule-swaps
	export DEPLOYMENT=debug
	if test ! -d $HOME_DIR/$DEPLOYMENT; then
		echo "Creating rule-swaps debug deployment"
		mkdir -p $HOME_DIR/$DEPLOYMENT
		chown rule-swaps $HOME_DIR/$DEPLOYMENT
		export LOG_FILE=$HOME_DIR/$DEPLOYMENT/log
		export PORT=1888
		touch $LOG_FILE
		chown rule-swaps $LOG_FILE
		export RUN_FILE=$HOME_DIR/$DEPLOYMENT/run
		cat run | envsubst > $RUN_FILE
		chown rule-swaps $RUN_FILE
		chmod 755 $RUN_FILE
		
		# Install service
		cat rule-swaps.service | envsubst > /etc/systemd/system/rule-swaps-$DEPLOYMENT.service
		systemctl daemon-reload
	fi
	
	# Try building
	mkdir -p /tmp/rule-swaps-debug/
	rsync -rt --exclude=.git --exclude=output --exclude=node_modules --delete \
		/srv/debug/rule-swaps/ /tmp/rule-swaps-debug/
	update_needed=$(set +e; cd /tmp/rule-swaps-debug/; make -q debug; echo $?)
	if test $update_needed = 1; then
		echo "Building rule-swaps debug deployment"
		(cd /tmp/rule-swaps-debug/; make debug)
		
		echo "Installing new build"
		rsync -rt /tmp/rule-swaps-debug/output/debug/ $HOME_DIR/$DEPLOYMENT/app/
	fi
	
	# Restart debug deployment
	echo "(Re-)Starting rule-swaps debug deployment"
	systemctl restart rule-swaps-$DEPLOYMENT

fi