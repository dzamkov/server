#!/bin/sh
# Automatically provisions or updates a server to deploy the applications defined by
# this project

set -e
cd $(dirname $0)

# Run per-project update
sh rule-swaps/update.sh